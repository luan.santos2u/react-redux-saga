export enum UserTypes {
  'SET_USER_NAME' = '@user/SET_USER_NAME',
  'GET_USERS_SUCCESS' = '@user/GET_USERS_SUCCESS',
  'GET_USERS_REQUEST' = '@user/GET_USERS_REQUEST',
  'REMOVE_USER_REQUEST' = '@user/REMOVE_USER_REQUEST',
  'REMOVE_USER_SUCCESS' = '@user/REMOVE_USER_SUCCESS'
}

export interface UserData {
  picture: { medium: string }
  name: { title: string, first: string }
  email: string
}

export interface UserState {
  hello: string
  name: string,
  users: UserData[]
}