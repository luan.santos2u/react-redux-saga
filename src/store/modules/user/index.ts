import { Reducer } from 'redux'

import { UserState, UserTypes } from './types'

const INITIAL_STATE: UserState = {
  hello: "Real2U",
  name: "Luan",
  users: []
}

const reducer: Reducer<UserState> = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case UserTypes.SET_USER_NAME:
      return {...state, name: action.payload.name}
    case UserTypes.GET_USERS_SUCCESS:
      return {...state, users: action.payload.users}
    case UserTypes.REMOVE_USER_SUCCESS:
      return {...state, users: action.payload.users}
    default:
      return state
  }
}

export default reducer