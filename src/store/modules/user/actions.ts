import { action } from 'typesafe-actions'
import { UserTypes, UserData } from './types'

export const setUserName = (name: string) => action(UserTypes.SET_USER_NAME, { name })
export const getUsersRequest = () => action(UserTypes.GET_USERS_REQUEST)
export const getUsersSuccess = (users: UserTypes[]) => action(UserTypes.GET_USERS_SUCCESS, { users })
export const removeUserRequest = (email: string) => action(UserTypes.REMOVE_USER_REQUEST, { email })
export const removeUserSuccess = (users: UserData[]) => action(UserTypes.REMOVE_USER_SUCCESS, { users })