import { call, all, takeLatest, put, select } from 'redux-saga/effects'
import api from '../../../services/api'

import { getUsersSuccess, removeUserSuccess } from './actions'
import { AnyAction } from 'redux'

import { ApplicationState } from '../../index'

import { UserTypes, UserData } from './types'


function* getUsers() {
  const usersData = yield call(api.get, '', 
     { params: { results: 10, nat: 'br' } })

  yield put(getUsersSuccess(usersData.data.results))
}

function* removeUser({ payload }: AnyAction) {
  const users = yield select((state: ApplicationState) => state.user.users)

  const updatedUsers = users.filter((user: UserData) => user.email !== payload.email)

  console.log(payload)

  yield put(removeUserSuccess(updatedUsers))
}

export default all([
  takeLatest(UserTypes.GET_USERS_REQUEST, getUsers),
  takeLatest(UserTypes.REMOVE_USER_REQUEST, removeUser)
])