import React from 'react'

import { Container, Picture, Info } from './styles'

interface UserData {
  picture: { medium: string }
  name: { title: string, first: string}
  email: string,
}

interface User {
  user: UserData
  handleDelete: (email: string) => void
}

const User: React.FC<User> = ({ user, handleDelete }: User) => {

  return (
    <Container key={user.email}>
      <Picture>
        <img src={user.picture.medium} alt=""/>
      </Picture>

      <Info>
        <strong>{user.name.first}</strong>
        <small>{user.email}</small>      
      </Info>

      <button onClick={() => handleDelete(user.email)} type="button">Delete</button>
    </Container>
  )
}

export default User