import styled from 'styled-components'

export const Container = styled.div`
  display: flex;
  align-items: center;
  padding: 20px;
  background: #fafafa;
  margin-bottom: 2px;

  button {
    margin-left: auto;
  }
`

export const Picture = styled.div`
  img {
    height: 60px;
    margin-right: 5px;
    border-radius: 50%;
  }
`

export const Info = styled.div`
  display: flex;
  flex-direction: column;
`