import React, { useEffect, useState } from 'react'
import { useSelector, useDispatch } from 'react-redux'

import { ApplicationState } from './store/index'

import { getUsersRequest, removeUserRequest } from './store/modules/user/actions'

import User from './components/User'

function ConsumingApi() {

  interface UserData {
    picture: { medium: string }
    name: { title: string, first: string}
    email: string
  }

  const dispatch = useDispatch()

  const users = useSelector((state: ApplicationState) => state.user.users)

  useEffect(() => {
    dispatch(getUsersRequest())
  }, [])

  function handleDelete(email: string) {
    alert("email to delete: " + email)
    dispatch(removeUserRequest(email))
  }

  return (
    <div>
      <h1>Consuming API</h1>

      {
        users.map(user => <User user={user} handleDelete={handleDelete} />)
      }

    </div>
  )
}

export default ConsumingApi